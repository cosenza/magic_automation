# MAGIC automation
Software installation and step-by-step instructions to run MAGIC (**M**achine-learning **A**ssisted **G**enomic and **I**maging **C**onvergence) experiments.
MAGIC is designed to run on Zeiss confocal microscopes controlled by [ZEN (blue edition)](https://www.zeiss.com/microscopy/en/products/software/zeiss-zen.html) Microscopy Software. Automation was tested on an LSM900 system, ZEN (blue edition, version 3.9) and Fiji (version 1.53e).

## repository content
The `./files` folder contains files that are needed to run a MAGIC experiment.  
There you will find:
- **zenblue_magic_automation.py** a macro written in IronPython, controlling microscope automation and integrating feedback from image analysis
- **MAGIC_photolabeling.jar** a Fiji plugin, which enables AutoMicTools communication with the image processing server 

## Installation

### Install magic_tools  
Install [magic_tools](https://git.embl.de/cosenza/magic_tools) package following the instructions on the repository.
Configure and test the server following the instructions there.  
The magic_tools image processing server does not need to run on the microscope computer. However, the server url address must be accessible from the microscope computer.  

---
### Installing Fiji and AutoMicTools plugin
Install [Fiji](https://imagej.net/software/fiji/) on the computer connected to the microscope, following the instructions on their website. Take note of where the software folder `Fiji.app` is located.

Install [AutoMicTools](https://git.embl.de/halavaty/AutoMicTools/) Fiji plug-in following the instructions that can be found [here](https://git.embl.de/halavaty/AutoMicTools/-/blob/master/Documentation/manual.md). Use the preferred mode of installation, via Fiji update sites.

Move the **MAGIC_photolabeling.jar** plug-in file in the `Fiji.app/jars/` folder.

---
### Install the microscope automation script in ZEN
Place the **zenblue_magic_automation.py** file into the macro folder of ZEN. If you don't know where it is located, you can always copy the content of the file and paste it in the macro editor, then save the macro.


## Testing micronuclei detection plugin
Launch the image processing server as described in the [magic_tools](https://git.embl.de/cosenza/magic_tools) repository.  

Launch Fiji. Then, in the Fiji menu bar go to  
`Plugins > Auto Mic Tools > ZenBlue Marco Cosenza > Photoactivation micronuclei via magic_tools`  

This will open a dialog that, for the purpose of testing, can be configured like this:  
- Image File Extension, set to `tif`
- Experiment folder, click on `Browse...` and select the `./test_folder` of this repository
Then click `OK`

A second dialog will open, configure it like this:
- Image processing server URL, set it to the server address, e.g. `http://127.0.0.1:10123/`
- Server endpoint, select the appropriate endpoint that was configured on the server, e.g. `mni_classify`

Now AutoMicTools will monitor the folder for new files to process
AutoMicTools will process only images that follow this naming convention: `Select--W0000--P0001-T0001`
**W** stands for well, **P** for position and **T** for time. The number increment accordingly with every acquired image.

In the `./example_images/` folder of this repository, there are 3 test images.  
Copy one image and paste it in the monitored folder `./test_folder`, manually or via command line:  
`cp ./example_images/Select--W0000--P0001-T0001.tif ./test_folder/Select--W0000--P0001-T0001.tif`  

AutoMicTools will detect the new file and send it for analysis. At the end it will visualize the detected cells.  
Moreover, it will save in the same folder a `.zip` file containing the ROIs and a `.json` file containing the feedback for microscope automation.  



## Launch a MAGIC experiment
Here are step-by-step instructions to launch a MAGIC experiment, assuming the installation and all tests run correctly.

**⚠️ Warning**  
<code style="color : red">Running the **zenblue_magic_automation.py** macro puts the microscope hardware and software at risk.</code>  
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

### before starting
Before launching a MAGIC experiment, make sure that all software was installed correctly by testing AutoMicTools and the magic_tools server.  
Moreover, make sure to have ready four ZEN experiment settings, for the tasks in the photolabeling routine:  
- **position experiment**  
in this experiment, only positions are considered to define the fields of view to image during the experiment
- **autofocus experiment**  
this is a line-scan stack (XZ line) with a 639   nm laser to identify the coverslip reflection to determine best focus  
- **imaging experiment**  
the imaging experiment is used to find target cells and define ROIs to be photolabeled
- **photolabeling experiment**  
the photolabeling experiment performs targeted illumination of the ROIs and takes two images before and after labeling
---
### start the server
This part can be set up on the microscope computer, a different computer or cluster node.
1. open a terminal
2. activate the python environment with magic_tools installed  
`conda activate magic`
3. navigate to the repository folder
`cd /path/to/magic_tools`
4. launch the server, in this example the server running locally (on the microscope computer)
`uvicorn magic_tools.server.main:app --port 10123`
---
### configure the server
Server configuration can be set up from any terminal, as long as we absolute paths are provided in a way that applies to the machine, where the server is running.
1. open a terminal
2. navigate to the repository folder  
`cd /path/to/magic_tools`
3. set server configuration  
`python set_server_configuration.py -c /path/to/magic_tools/resources/configurations/config_micronuclei_MCF10A_H2B-Dendra2.json -s http://127.0.0.1 -p 10123`
---
### start AutoMicTools
Start AutoMicTools on the microscope computer, so it has access to the acquired images and can coordinate with both the microscope software and the image processing server.  
1. open Fiji
2. in menu bar go to  
`Plugins > Auto Mic Tools > ZenBlue Marco Cosenza > Photoactivation micronuclei via magic_tools`  
3. set the first dialog:  
    - Image File Extension, set to `czi`  
    - Experiment folder, click on `Browse...` select your `/path/to/experiment_folder`
    - Click OK
4. set the second dialog:
    - Image processing server URL, `http://127.0.0.1:10123/`
    - Server endpoint, `mni_classify`

---
### start the experiment
In the last part, start ZEN, set the sample on the microscope and run the automated experiment.  
1. open ZenBlue
2. load the experiment containing the positions to image (or define a new one)
3. align the positions to the sample carrier on the microscope stage
4. open the macro environment, from the menu bar click on `Macro > Macro Editor`  

**in the macro window**

5. open the `zenblue_magic_automation.py` script
6. set the experiment folder to the same `/path/to/experiment_folder` defined in AutoMicTools
7. set the manually defined variables with the correct experiment names
8. press run to start the experiment
---
### monitor the experiment
Locally, monitor the experiment by closely watching the microscope hardware, as well as AutoMicTools displaying image analysis results and printing messages to the console.  
Remotely, monitor the experiment by looking at the image processing server dashboard http://127.0.0.1:10123/dashboard  


## Contributors

- Marco Raffaele Cosenza (maintainer and developer)
- Aliaksandr Halavatyi (developer, AutoMicTools and zenblue automation)

## Contact

Please, contact Marco Raffaele Cosenza (marco.cosenza(at)embl.de) in case you have any questions.